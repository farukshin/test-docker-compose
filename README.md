### test-docker-compose

Получить иходный код проекта, запустить в контейнерах через docker-compose
```
git clone https://gitlab.com/farukshin/test-docker-compose.git
cd test-docker-compose
docker-compose up
docker image ls
```

Добавим в файл docker-compose монтирование *volumes:* *- .:/code*
```
docker-compose up
```

Обновим код приложения. Заменим строку вывода сообщения на *return 'Hello from Docker! I have been seen {} times.\n'.format(count)*
Докер образы обновятся автоматический.

Что еще интересного?
```
docker-compose up -d
docker-compose ps
docker-compose run web env
docker-compose stop
docker-compose down --volumes
```